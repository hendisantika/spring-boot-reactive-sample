package com.hendisantika.router;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/15/22
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class CustomerAPIRouterConfig {

    @Bean
    public RouterFunction<ServerResponse> customerRoutes(CustomerAPIHandler handler) {
        return
                RouterFunctions
                        .route(RequestPredicates.GET("/f/customers"),
                                handler::handleGetAllCustomers)
                        .andRoute(RequestPredicates.GET("/f/customers/{customerId}"),
                                handler::handleGetCustomerById);
    }
}
