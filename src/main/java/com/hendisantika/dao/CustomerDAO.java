package com.hendisantika.dao;

import com.hendisantika.model.Customer;
import com.hendisantika.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/14/22
 * Time: 06:39
 * To change this template use File | Settings | File Templates.
 */
@Component
@RequiredArgsConstructor
public class CustomerDAO {

    private final CustomerRepository customerRepository;

    public Flux<Customer> getCustomers() {
        Flux<Customer> customers = customerRepository.findAll();
        return customers;
    }

    public Mono<Customer> getCustomerById(String id) {
        Mono<Customer> customerMono = customerRepository.findById(id);
        return customerMono;
    }

}
