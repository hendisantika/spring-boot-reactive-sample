package com.hendisantika.repository;

import com.hendisantika.model.Customer;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/14/22
 * Time: 06:38
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface CustomerRepository extends ReactiveMongoRepository<Customer, String> {
}
