# Get String from Mono in reactive java

### 1. Overview

In this article, we will learn to Get String from Mono<String> in reactive java. To know about Mono or Flux reactive
stream, refer to this article.

A Flux object represents a reactive sequence of 0..N items, whereas a Mono object represents a single value or an
empty (0..1) result.

As you know, Mono is an asynchronous call that executes in a non-blocking way.

### 2. Get String from Mono<String> in reactive java

Have you ever wondered how to extract the String value from Mono<String>. Let’s see both blocking and non-blocking ways
to retrieve String from the Mono<String>.

However, it is not recommended to use the blocking way to get string from Mono as it is against the reactive programming
principle.

### 2.1. Blocking way to get String from Mono<String>

You can use the block() method to block the current thread indefinitely and wait for Mono to complete. If the Mono
completes, then this method either returns the original value or null (Mono is empty).

In case of any errors, then the exception is rethrown.

```java

Mono<String> getMono() {
    return Mono.just("Example of Mono");
}

String result = getMono().block();
System.out.println(result);
/* prints
Example of Mono */
```

### 2.1.1. Block with a timeout

Rather than waiting indefinitely for the Mono to complete using the block() method, you can specify a timeout to wait:

`public T block(Duration timeout)`

The above block method accepts timeout duration as input and waits until the Mono completes or the timeout expires. It
throws a RuntimeException when the timeout expires.

```java
String result = getMono().block(Duration.ofMillis(10000));
System.out.println(result);
/* prints
Example of Mono */
```

### 2.1.2. blockOptional

If the Mono completes, then the block method returns the original value or null if empty. Instead of getting null value,
you can get Optional<String> by using the blockOptional method.

`Optional<String> result = Mono.<String>empty().blockOptional();`

### 3. Non-blocking way to get String from Mono<String>

You can retrieve the result from Mono in a non-blocking way by subscribing to a Consumer that will consume all the
sequences. The Consumer code block executes asynchronously only after Mono completes.

`Disposable subscribe(Consumer<? super T> consumer)`

#### 3.1. Chain multiple consumers

You can chain multiple consumers using doOnNext method. Once Mono completes, the result is passed downstream to the
chained consumers for further processing. Note that the doOnNext are consumers of the result of Mono and not publishers,
meaning they don’t pass (publish) any value downstream.

For example, both the doOnNext methods and subscribe methods consume the result of Mono.

```java
getMono().doOnNext(r -> {
if (result != null) {
String value = result.toUpperCase();
System.out.println(value);
}
})
.doOnNext(r -> {
if (result != null) {
String value = result.toLowerCase();
System.out.println(value);
}
})
.subscribe(System.out::println);
/* prints
EXAMPLE
example
Example */
```

### 4. Conclusion

To sum up, we have learned to retrieve the String from Mono<String> in reactive Java. 