package com.hendisantika.controller;

import com.hendisantika.model.CustomerAddress;
import com.hendisantika.repository.CustomerAddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/15/22
 * Time: 22:11
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequiredArgsConstructor
public class CustomerAddressRestController {

    private final CustomerAddressRepository customerAddressRepository;

    @GetMapping(path = "/customers/{customerId}/addresses", params = {"type"})
    public Flux<CustomerAddress> handleGetAddressesByIdAndType(@PathVariable String customerId,
                                                               @RequestParam String type) {
        Flux<CustomerAddress> addressFlux = customerAddressRepository.findByCustomerIdAndType(customerId, type);
        return addressFlux;
    }

}
