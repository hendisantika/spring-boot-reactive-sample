package com.hendisantika.repository;

import com.hendisantika.model.CustomerAddress;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-reactive
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/14/22
 * Time: 06:37
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface CustomerAddressRepository extends ReactiveMongoRepository<CustomerAddress, String> {

    Flux<CustomerAddress> findByCustomerIdAndType(String customerId, String type);
}
